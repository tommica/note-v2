/*global $:false, window:false, t:false */
'use strict';

var note = note;

$(function() {
  // Initialize the storage
  if( note.check() === false ) {
    note.put( {} );
  }

  // Get the window size, used for positioning the notes & other vars
  var window_width = window.innerWidth;
  var window_height = window.innerHeight;
  var new_note_args = {width: 300, height: 200, left: function() { return parseInt( (Math.random() * (window_width - this.width)), null); }, top: function() { return parseInt( (Math.random() * (window_height - this.height)), null); }};
  var note_template = '<div class="note" id="id_{id}" style="left: {left}px; top: {top}px; width: {width}px; height: {height}px;"><div class="header"><div class="dragBar"></div><a href="#{id}" class="remove"><img src="img/remove.png" alt="Remove" /></a></div><textarea>{content}</textarea><div class="footer"><img src="img/resize.png" alt="" /></div></div>';
  var $body = $('body');

  // Render note
  function render_note(id, left, top, width, height, content, note_template) {
    var note_html = t(note_template, {id: id, left: left, top: top, width: width, height: height, content: content});
    $body.append( $(note_html).draggable({handle: '.header .dragBar'}).resizable({ create: function(e, $this) { $(this).css({ position: "absolute" }); } }) );
    return true;
  }


  //Focus on the searchbox
  $('.search .input').focus();

  // Get the notes
  var notes = note.get();

  // Render all notes
  for(var noteID in notes) {
    var width = notes[noteID].width;
    var height = notes[noteID].height;
    var left = notes[noteID].left;
    var top = notes[noteID].top;
    var content = notes[noteID].content;

    render_note(noteID, left, top, width, height, content, note_template);
  }


  // Add new note
  $('.addNew').bind('click', function() {
    // Generate a random id
    var id = note.generateRandomId();

    // Add a new note to the notes object
    var left = new_note_args.left();
    var top = new_note_args.top();
    notes[id] = note.newNote(id, new_note_args.width, new_note_args.height, left, top);

    //Display that note
    render_note(id, left, top, new_note_args.width, new_note_args.height, 'content', note_template);


    return false;
  });
  
  // Remove note
  $body.delegate('.remove','click', function() {
    // Get id ( #12345 => 12345 )
    var id = $(this).attr('href').substr(1);

    // Remove it from notes
    delete notes[id];

    // Remove that note from display
    $body.find('#id_'+id).remove();

    return false;
  });


  //Save every half second
  (function loopSave() {
    $body.find('.note').each(function(i,e) {
      var $target = $(e);
      var noteID = $target.attr('id').substr(3);
      notes[noteID].width = $target.width();
      notes[noteID].height = $target.height();
      notes[noteID].left = $target.offset().left;
      notes[noteID].top = $target.offset().top;
      notes[noteID].content = $target.find('textarea')[0].value;
    });

    note.put(notes);
    setTimeout(loopSave, 700);
  }());
});
