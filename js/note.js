'use strict';

var store = store;

var note = (function() {
  var STORAGE_ID = 'note_storage';

  return {
    // Function GET
    // Parses a JSON string - returns an object
    get: function() {
      var contents = store.get(STORAGE_ID);
      return JSON.parse(contents);
    },

    // Function PUT
    // Stringifies the object and stores it - return true
    put: function(object) {
      object = object || {};
      var str = JSON.stringify(object);
      store.set(STORAGE_ID, str);
      return true;
    },

    // Function CHECK
    // Checks that storage is set - returns boolean
    check: function() {
      var contents = store.get(STORAGE_ID);
      var rtr = typeof(contents) === 'string' ? true : false;
      return rtr;
    },

    // Function GENERATERANDOMID
    // Returns a random string of numbers
    generateRandomId: function() {
      var id = parseInt( (Math.random() * 1000000), null );
      return id;
    },
    
    // Function NEWNOTE
    // Returns a new object
    newNote: function(id, width, height, left, top) {
      id = id || this.generateRandomId();
      width = width || 300;
      height = height || 200;
      left = left || 100;
      top = top || 100;

      return {
        id: id,
        width: width,
        height: height,
        left: left,
        top: top,
        content: 'content'
      };
    }
  };

}());
